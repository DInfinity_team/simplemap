package com.dinfinity.simple.map;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;

public class SimpleMapActivity extends Activity {

	MapView mMapView;

	ArcGISTiledMapServiceLayer streetLayer;
	ArcGISTiledMapServiceLayer physicalLayer;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mMapView = (MapView) findViewById(R.id.map);

		streetLayer = new ArcGISTiledMapServiceLayer(this.getResources()
				.getString(R.string.WORLD_STREETS_URL));
		physicalLayer = new ArcGISTiledMapServiceLayer(this.getResources()
				.getString(R.string.WORLD_PHYSICAL_URL));

		mMapView.addLayer(physicalLayer);
		mMapView.addLayer(streetLayer);

		streetLayer.setVisible(true);
		physicalLayer.setVisible(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.streets_menu_item:
			streetLayer.setVisible(true);
			physicalLayer.setVisible(false);
			return true;
		case R.id.physical_menu_item:
			streetLayer.setVisible(false);
			physicalLayer.setVisible(true);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mMapView.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mMapView.unpause();
	}

}